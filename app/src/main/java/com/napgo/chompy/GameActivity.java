package com.napgo.chompy;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import java.util.List;

/**
 * Created by Dariusz on 2015-12-03.
 */
public class GameActivity extends Activity implements SensorEventListener {

    float[] orientationVals = new float[3];
    float[] rotMat = new float[9];
    private SensorManager mSensorManager;
    Sensor rotationVectorSensor;
    ImageView chompyImageView;

    private float currentAngle = 0f;
    private float startingAngle = 0f;
    private boolean angleInitialized = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        chompyImageView = (ImageView) findViewById(R.id.chompyImageView);
        setupSensor();
    }

    private void setupSensor() {
        if (mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR) != null) {
            List<Sensor> rotSensors = mSensorManager.getSensorList(Sensor.TYPE_ROTATION_VECTOR);
            for (int i = 0; i < rotSensors.size(); i++) {
                if (!rotSensors.get(i).getVendor().contains("Google Inc.")) {
                    rotationVectorSensor = rotSensors.get(i);
                }
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, rotationVectorSensor, SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            // calculate th rotation matrix
            SensorManager.getRotationMatrixFromVector(rotMat, event.values);

            // get the azimuth value (orientation[0]) in degree
            float angle = -1 * (int) (Math.toDegrees(SensorManager.getOrientation(rotMat, orientationVals)[0]) + 360) % 360;

            if (angleInitialized)
                setupAnimation(angle);
            else {
                startingAngle = angle;
                angleInitialized = true;
            }
        }
    }

    private void setupAnimation(float angle) {
        RotateAnimation rotateAnimation = new RotateAnimation(
                currentAngle,
                -(startingAngle - angle),
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(210);
        rotateAnimation.setFillAfter(true);
        chompyImageView.startAnimation(rotateAnimation);
        currentAngle = -(startingAngle - angle);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}

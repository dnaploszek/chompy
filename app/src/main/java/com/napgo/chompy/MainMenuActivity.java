package com.napgo.chompy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainMenuActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
    }

    public void mainMenuButtonClicked(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.newGameButton:
                intent = new Intent(this, GameActivity.class);
                startActivity(intent);
                break;

            case R.id.settingsButton:
                intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;

            case R.id.exitGameButton:
                this.finish();
                System.exit(0);
                break;
        }
    }
}
